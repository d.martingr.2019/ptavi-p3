
import sys
import smil


def main(possibleFileName):
    try:
        fileName = str(possibleFileName)
    except:
        print(f'Usage: python3 elements.py <file>')
    else:
        file = smil.SMIL(fileName)
        for element in file.elements():
            print(element.name())


if __name__ == '__main__':
    main(sys.argv[1])
