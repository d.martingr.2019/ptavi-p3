#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import unittest

import elements

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

result_karaoke = """smil
head
layout
root-layout
region
region
region
body
par
img
img
audio
textstream
audio
"""

class TestMain(unittest.TestCase):

    def setUp(self):
        os.chdir(parent_dir)
        self.stdout = StringIO()

    def test_karaoke(self):
        with contextlib.redirect_stdout(self.stdout):
            elements.main(os.path.join("karaoke.smil"))
        output = self.stdout.getvalue()
        self.assertEqual(result_karaoke, output)

if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
